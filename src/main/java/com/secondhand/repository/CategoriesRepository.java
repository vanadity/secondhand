package com.secondhand.repository;

import com.secondhand.model.Categories;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriesRepository extends JpaRepository<Categories, Integer> {

    public Categories findByCategoryName(String categoryName);

    public Categories findByCategoryId(Integer categoryId);
}
