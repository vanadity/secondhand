package com.secondhand.repository;

import com.secondhand.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {

    Boolean existsByEmail(String email);

    public Users findByUserId(Integer userId);
    public Users findByUsername(String username);
    public Users findByEmail(String email);
}
