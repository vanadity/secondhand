package com.secondhand.repository;

import com.secondhand.model.Offers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OffersRepository extends JpaRepository <Offers, Integer> {

    @Query("select o from offers o where o.users.userId=?1")
    List<Offers> findByUserId(Integer userId);

    @Query("select o from offers o where o.users.userId=?1 and o.products.productId=?2")
    Optional<Offers> findByUserIdAndProduct(Integer userId, Integer productId);
}
