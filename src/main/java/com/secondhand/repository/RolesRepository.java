package com.secondhand.repository;

import com.secondhand.enumeration.ERole;
import com.secondhand.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Integer> {

    Optional<Roles> findByName(ERole name);
}
