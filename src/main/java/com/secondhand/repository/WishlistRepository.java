package com.secondhand.repository;

import com.secondhand.model.Wishlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface WishlistRepository extends JpaRepository <Wishlist, Integer> {
    @Modifying
    @Query(value ="delete from wishlist where product_id=?1 and user_id=?2" ,nativeQuery = true)
    void deleteWishlistByProductIdAndUserId(Integer productId, Integer userId);

//    @Query(value = "select * from wishlist where user_id=?1 order by created_on desc", nativeQuery =
//            true)
//    List<Wishlist> findAllByUserIdOrderBy

}
