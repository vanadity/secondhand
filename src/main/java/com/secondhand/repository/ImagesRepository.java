package com.secondhand.repository;

import com.secondhand.model.Images;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImagesRepository extends JpaRepository <Images, Integer> {

    public Images findByImageId(Integer ImageId);
}
