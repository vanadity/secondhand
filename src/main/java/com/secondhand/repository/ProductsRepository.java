package com.secondhand.repository;

import com.secondhand.model.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Integer> {

    public Products findByProductId(Integer productId);

    @Query("select p from products p where p.users.userId = ?1")
    List<Products> findProductByUserId(Integer users);

    Page<Products> findAll(Pageable pageable);

    Page<Products> findByProductNameContainingIgnoreCaseAndPriceBetween
            (String productName, Long priceMin, Long priceMax,
             Pageable pageable);

    @Query("select  p from products p " +
            "where p.categories.categoryId =:categoryId ")
    Page<Products> findByCategories(Integer categoryId, Pageable pageable);
}