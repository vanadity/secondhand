package com.secondhand.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

    private Integer categoryId;
    private Integer userId;
    private String productName;
    private Boolean productStatus;
    private Long price;
    private String description;
}
