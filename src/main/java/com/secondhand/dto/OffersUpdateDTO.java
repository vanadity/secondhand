package com.secondhand.dto;

import com.secondhand.enumeration.OfferStatus;
import lombok.Data;

@Data
public class OffersUpdateDTO {

    private Integer offerId;
    private OfferStatus offerStatus;
}
