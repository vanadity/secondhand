package com.secondhand.dto;

import lombok.Data;

@Data
public class OffersDTO {

    private String username;
    private Integer productId;
    private Long offerPrice;
}
