package com.secondhand.dto;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfileUserDTO implements Serializable {

    private Integer userId;
    private String username;
    private String email;
    private String city;
    private String address;
    private Long phoneNumber;
    private String imageUser;
}
