package com.secondhand.enumeration;

public enum OperationStatus {

    SUCCESS("Success"),
    FAILURE("Failure"),
    COMPLETED("Completed"),
    NOT_COMPLETED("Not Completed"),
    NOT_FOUND("Not Found"),
    FOUND("Found");

    private final String name;

    OperationStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
