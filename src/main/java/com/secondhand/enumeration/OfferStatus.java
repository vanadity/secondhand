package com.secondhand.enumeration;

public enum OfferStatus {

    Available("Available"),
    Accepted("Accepted"),
    Rejected("Rejected");

    private String name;

    OfferStatus(String name) {
        this.name = name;
    }
}
