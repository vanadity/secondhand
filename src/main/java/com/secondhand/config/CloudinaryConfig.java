package com.secondhand.config;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import com.secondhand.service.CloudinaryService;
import com.secondhand.service.impl.CloudinaryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class CloudinaryConfig {

    @Bean
    public Cloudinary cloudinary() {
        return new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "di4lwmmpn",
                "api_key", "149238174211388",
                "api_secret", "huh-aMLZYAi8aAU1Evj0X1oataQ"));
    }

    @Bean
    public CloudinaryService cloudinaryService() {
        return new CloudinaryServiceImpl(cloudinary());
    }
}
