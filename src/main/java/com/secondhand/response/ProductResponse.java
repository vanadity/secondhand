package com.secondhand.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ProductResponse {
    private String productName;
    private Boolean productStatus;
    private Long price;
    private String description;
    private String categoryName;
    private String username;
    private String createdDate;
    private List<String> imageProduct;
}
