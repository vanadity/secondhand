package com.secondhand.response;

import lombok.Data;

@Data
public class ErrorResponse {
    private String message;
}
