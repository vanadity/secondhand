package com.secondhand.response;

import lombok.Data;

@Data
public class UploadResponse {
    String message;
    String url;
}
