package com.secondhand.response;

import lombok.Data;

@Data
public class UploadResponse1 {
    String message;
    String url[];
}
