package com.secondhand.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProfileUserResponse {

    private Integer userId;
    private String username;
    private String city;
    private String address;
    private Long phoneNumber;
    private String url;
}
