package com.secondhand.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CompletedResponse {
    private String message;
    private String status;
}
