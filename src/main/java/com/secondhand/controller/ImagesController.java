package com.secondhand.controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.secondhand.model.Images;
import com.secondhand.response.UploadResponse;
import com.secondhand.repository.ImagesRepository;
import com.secondhand.response.UploadResponse1;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@Tag(name = "Images", description = "api endpoint for image by using images entity in secondhand" +
        "database")
@RestController
@RequestMapping("/images")
//@CrossOrigin(origins = {"http://localhost:3000", "*"}, allowedHeaders = "*")
public class ImagesController {

    @Autowired
    private ImagesRepository imagesRepository;

    private static final Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
            "cloud_name", "di4lwmmpn",
            "api_key", "149238174211388",
            "api_secret", "huh-aMLZYAi8aAU1Evj0X1oataQ"));

    @Operation(summary = "this method function to testing upload image")
    @PostMapping("/upload")
    public ResponseEntity<UploadResponse> uploadImage(@RequestParam("image") MultipartFile image) throws IOException {
        UploadResponse1 response = new UploadResponse1();
        File file = new File(image.getOriginalFilename());
        FileOutputStream os = new FileOutputStream(file);
        os.write(image.getBytes());
        os.close();
        Map result = cloudinary.uploader().upload(file,
                ObjectUtils.asMap("image_id", "image_name"));
        response.setMessage("Upload successful!");
        String[] url = new String[1];
        url[0] = result.get("url").toString();
        response.setUrl(url);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @Operation(summary = "this method function to testing multiple image")
    @Transactional
    @GetMapping(
            value = "/download/{imageId}",
            produces = MediaType.IMAGE_JPEG_VALUE
    )
    public ResponseEntity<byte[]> getFile(@PathVariable("imageId") Integer imageId) {
        Images images = imagesRepository.findByImageId(imageId);
        InputStream in = getClass().getResourceAsStream(images.getImageName());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" +
                        images.getImageName() + "\"")
                .body(images.getImageFile());
    }

    @PostMapping("/upload/multiple")
    public ResponseEntity<UploadResponse> uploadMultiple(@RequestParam("files") MultipartFile[] files) throws IOException {
        UploadResponse1 response = new UploadResponse1();
        Integer size = files.length;
        String[] url = new String[size];
        for(int i = 0; i < size; i++) {
            File file = new File(files[i].getOriginalFilename());
            FileOutputStream os = new FileOutputStream(file);
            os.write(files[i].getBytes());
            os.close();
            Map result = cloudinary.uploader().upload(file,
                    ObjectUtils.asMap("image_id", "image_name"));
            url[i] = result.get("url").toString();
        }
        response.setMessage("Upload successful!");
        response.setUrl(url);

        return new ResponseEntity(response, HttpStatus.ACCEPTED);
    }
}

/**
 * upload to database
 * //        Images images = new Images();
 * //        System.out.println(image.getContentType());
 * //        if(image.getContentType().contains("image")) {
 * //        }
 * //        images.setImageFile(image.getBytes());
 * //        images.setImageName(image.getOriginalFilename());
 * //        imagesRepository.save(images);
 * */