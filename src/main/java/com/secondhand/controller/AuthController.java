package com.secondhand.controller;

import com.secondhand.config.JwtUtils;
import com.secondhand.enumeration.ERole;
import com.secondhand.model.*;
import com.secondhand.repository.RolesRepository;
import com.secondhand.repository.UsersRepository;
import com.secondhand.response.JwtResponse;
import com.secondhand.response.MessageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Tag(name="Authentication", description = "api endpoint for authentication by using relations " +
        "users and roles entity in secondhand")
@RestController
@RequestMapping("/api/auth")
//@CrossOrigin(origins = {"http://localhost:3000", "*"}, allowedHeaders = "*")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    RolesRepository rolesRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtils jwtUtils;

    public AuthController() {
    }

    public AuthController(AuthenticationManager authenticationManager, UsersRepository usersRepository,
                          JwtUtils jwtUtils, RolesRepository rolesRepository, PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.usersRepository = usersRepository;
        this.jwtUtils = jwtUtils;
        this.rolesRepository = rolesRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema (example =
                                    "{" +
                                    "\"email\":\"user@email.com\"," +
                                    "\"password\":\"user\"" +
                                    "}"))
                    }),
            @ApiResponse(responseCode = "400", description = "Failed!.",
                    content = { @Content})})


    @Operation(summary = "this method function to log in user")
    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody Map<String, Object> login) {
        Users users = usersRepository.findByEmail(login.get("email").toString());

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(users.getUsername(), login.get("password"))
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(),
                roles));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"username\":\"user\"," +
                                    "\"email\":\"user@email.com\"," +
                                    "\"password\":\"user\"" +
                                    "}"))
                    }),
            @ApiResponse(responseCode = "400", description = "Failed!.",
                    content = { @Content})})

    @Operation(summary = "this method function to register user")
    @PostMapping("/signup")
    public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        Boolean emailExist = usersRepository.existsByEmail(signupRequest.getEmail());
        if(Boolean.TRUE.equals(emailExist)) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Error: Email is already taken!"));
        }

        Users users = new Users(signupRequest.getUsername(), signupRequest.getEmail(),
                passwordEncoder.encode(signupRequest.getPassword()));
        Set<String> strRoles = signupRequest.getRole();
        Set<Roles> roles = new HashSet<>();
        if (strRoles == null) {
            Roles role = rolesRepository.findByName(ERole.BUYER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found"));
            roles.add(role);
        } else {
            strRoles.forEach(role -> {
                Roles roles1 = rolesRepository.findByName(ERole.valueOf(role)).
                        orElseThrow(() -> new RuntimeException("Error : Role " + role + "is not found"));
                roles.add(roles1);
            });
        }
        users.setRoles(roles);
        usersRepository.save(users);
        return ResponseEntity.ok(new MessageResponse("User registered successfully"));
    }
}