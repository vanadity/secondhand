package com.secondhand.controller;

import com.secondhand.model.Categories;
import com.secondhand.model.Users;
import com.secondhand.service.CategoriesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name ="Categories", description = "api endpoint for category by using categories entity in " +
        "secondhand database")
@RestController
@RequestMapping("/categories")
//@CrossOrigin(origins = {"http://localhost:3000", "*"}, allowedHeaders = "*")
@Controller
public class CategoriesController {

    @Autowired
    private CategoriesService categoriesService;

    @Operation(summary = "this method function to get category")
    @GetMapping("/get-category/{categoryName}")
    public ResponseEntity<RestTemplate> getCategoryByCategoryName(@PathVariable String categoryName) {
        Categories categories = categoriesService.getCategoryByCategoryName(categoryName);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("category name:", categories.getCategoryName());

        return new ResponseEntity(responseBody, HttpStatus.OK);
    }

    @Operation(summary = "")
    @PostMapping("/add-category")
    public ResponseEntity<RestTemplate> addCategory(@RequestBody Categories categories) {
        categoriesService.addCategory(categories);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "")
    @PutMapping("/update-category")
    public ResponseEntity<RestTemplate> updateCategory (@RequestBody Categories categories) {
        categoriesService.updateCategory(categories);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @Operation(summary = "")
    @DeleteMapping("/delete-category/{categoryId}")
    public ResponseEntity<RestTemplate> deleteCategory (@PathVariable Integer categoryId) {
        categoriesService.deleteCategoryByCategoryId(categoryId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @Operation(summary = "this method function to get all category data")
    @GetMapping(value = "/get-all-category")
    public ResponseEntity<RestTemplate> getAllCategory() {
        List<Categories> allCategories = categoriesService.getAllCategories();
        return new ResponseEntity(allCategories, HttpStatus.OK);
    }
}
