package com.secondhand.controller;

import com.secondhand.dto.OffersDTO;
import com.secondhand.dto.OffersUpdateDTO;
import com.secondhand.mapping.OfferMapper;
import com.secondhand.model.Offers;
import com.secondhand.repository.OffersRepository;
import com.secondhand.response.BaseResponse;
import com.secondhand.service.OffersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Tag(name = "Offer", description = "api endpoint for offer by using offer entity in secondhand " +
        "database")
@RestController
@RequestMapping("/offer")
//@CrossOrigin(origins = {"http://localhost:3000", "*"}, allowedHeaders = "*")
@Controller
public class OffersController {

    @Autowired
    private OffersService offersService;

    @Autowired
    private OffersRepository offersRepository;

    @Operation(summary = "this method function to add offer")
    @PostMapping(value = "/add-offer")
    public ResponseEntity<RestTemplate> addOffer(@ModelAttribute OffersDTO offersDTO, Authentication authentication) {
        BaseResponse baseResponse = offersService.addOffers(offersDTO, authentication);

        return new ResponseEntity(baseResponse, HttpStatus.CREATED);
    }

    @Operation(summary = "this method function to update offer ")
    @PutMapping(value = "/update-offer")
    public ResponseEntity<RestTemplate> updateOffer(@ModelAttribute OffersUpdateDTO offersUpdateDTO, Authentication authentication) {
        OfferMapper offerMapper = offersService.updateOffers(offersUpdateDTO, authentication);

        return new ResponseEntity(offersUpdateDTO, HttpStatus.ACCEPTED);
    }

    @Operation(summary = "this method function to get offer")
    @GetMapping(value = "/get-offer")
    public ResponseEntity<RestTemplate> getOfferByUserId(@PathVariable Integer userId) {
        List<OfferMapper> offersByUserId = offersService.getOffersByUserId(userId);

        return new ResponseEntity(offersByUserId, HttpStatus.OK);
    }
}
