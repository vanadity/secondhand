package com.secondhand.controller;

import com.secondhand.dto.ProfileUserDTO;
import com.secondhand.model.Products;
import com.secondhand.model.Users;
import com.secondhand.response.ProfileUserResponse;
import com.secondhand.response.UploadResponse;
import com.secondhand.service.UsersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name="Users", description = "api endpoint for user by using user entity in secondhand database")
@RestController
@RequestMapping("/users")
//@CrossOrigin(origins = {"http://localhost:3000", "*"}, allowedHeaders = "*")
@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"userid\":\"1\","+
                                    "\"username\":\"user\","+
                                    "\"email\":\"user@email.com\"," +
                                    "\"password\":\"user\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to get user data by username")
    @GetMapping("/get-user-by-username/{username}")
    public ResponseEntity<RestTemplate> getUserByUsername(@Schema(example = "input username that " +
            "was created") @PathVariable String username) {
        Users users = usersService.getUserByUsername(username);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("userid", users.getUserId());
        responseBody.put("name", users.getUsername());
        responseBody.put("email", users.getEmail());
        return new ResponseEntity(responseBody, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"userId\":\"1\","+
                                    "\"username\":\"user\","+
                                    "\"email\":\"user@email.com\"," +
                                    "\"password\":\"user\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to get user data by id")
    @GetMapping("/get-user-by-id/{userId}")
    public ResponseEntity<RestTemplate> getUserById(@Schema(example = "input user id that " +
            "was created") @PathVariable Integer userId) {
        Users users = usersService.getUserById(userId);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("userId", users.getUserId());
        responseBody.put("name", users.getUsername());
        responseBody.put("email", users.getEmail());
        return new ResponseEntity(responseBody, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"userId\":\"1\","+
                                    "\"username\":\"user\","+
                                    "\"email\":\"user@email.com\"," +
                                    "\"password\":\"user\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to get user data by email")
    @GetMapping("/get-user-by-email/{email}")
    public ResponseEntity<RestTemplate> getUserByEmail(@Schema(example = "input email that " +
            "was created") @PathVariable String email) {
        Users users = usersService.getUserByEmail(email);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("userId", users.getUserId());
        responseBody.put("name", users.getUsername());
        responseBody.put("email", users.getEmail());
        return new ResponseEntity(responseBody, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"userId\":\"1\","+
                                    "\"username\":\"user\","+
                                    "\"email\":\"user@email.com\"," +
                                    "\"password\":\"user\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to add user but cannot access the login feature")
    @PostMapping("/add-user")
    public ResponseEntity<RestTemplate> addUser(@RequestBody Users users) {
        usersService.addUser(users);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"email\":\"user@email.com\","+
                                    "\"username\":\"user\"," +
                                    "\"city\":\"kota bandung\"," +
                                    "\"address\":\"jln dago\"," +
                                    "\"phoneNumber\":\"081234567890\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to update user profile")
    @PutMapping(value = "/update-user",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RestTemplate> updateUser(@ModelAttribute ProfileUserDTO profileUserDTO,
                                                   @RequestParam MultipartFile image) {
        ProfileUserResponse response = usersService.updateUserbyId(profileUserDTO, image);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"userId\":\"1\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to delete user")
    @DeleteMapping("/delete-user/{userId}")
    public ResponseEntity<RestTemplate> deleteUserByUserId(@Schema(example = "input user id that " +
            "was created") @PathVariable Integer userId) {
        usersService.deleteUserByUserId(userId);
        return new ResponseEntity("user id " + userId + " has been deleted", HttpStatus.ACCEPTED);
    }

    @Operation(summary = "this method function to get all user data")
    @GetMapping(value = "/get-all-user")
    public ResponseEntity<RestTemplate> getAllUsers() {
        List<Users> allUsers = usersService.getAllUsers();
        return new ResponseEntity(allUsers, HttpStatus.OK);
    }
}
