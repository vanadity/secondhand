package com.secondhand.controller;

import com.secondhand.exceptions.IllegalException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.handler.ResponseStatusExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
//@CrossOrigin(origins = {"http://localhost:3000", "*"}, allowedHeaders = "*")
public class ControllerAdvisor extends ResponseStatusExceptionHandler {

    @ExceptionHandler(IllegalException.class)
    public ResponseEntity<Object> illegalActionDataHandler(IllegalException illegalException) {
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("message", illegalException.getMessage());
        response.put("code", HttpStatus.BAD_REQUEST.value());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Object> usernameNotFoundHandler(UsernameNotFoundException usernameNotFoundException) {
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("message", usernameNotFoundException.getMessage());
        response.put("code", HttpStatus.NOT_FOUND.value());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
}
