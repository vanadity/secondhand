package com.secondhand.controller;

import com.secondhand.dto.ProductDTO;
import com.secondhand.enumeration.OperationStatus;
import com.secondhand.model.Products;
import com.secondhand.model.Users;
import com.secondhand.repository.ProductsRepository;
import com.secondhand.response.BaseResponse;
import com.secondhand.response.CompletedResponse;
import com.secondhand.response.ErrorResponse;
import com.secondhand.response.ProductResponse;
import com.secondhand.service.ProductsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name = "Product", description = "api endpoint for product by using product entity in " +
        "secondhand database")
@RestController
@RequestMapping("/product")
//@CrossOrigin(origins = {"http://localhost:3000", "*"}, allowedHeaders = "*")
@Controller
public class ProductsController {

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private ProductsService productsService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"productId\":\"1\","+
                                    "\"categoryId\":\"1\"," +
                                    "\"productName\":\"ban\"," +
                                    "\"productStatus\":\"true\"," +
                                    "\"price\":\"500000\"," +
                                    "\"description\":\"ban dengan kualitas terbaik\"," +
                                    "\"imageProduct\":\"string\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to get product data by product id")
    @GetMapping("/get-product-by-productname/{productId}")
    public ResponseEntity<RestTemplate> getProductByProductId(@PathVariable Integer productId) {
        Products products = productsService.getProductByProductId(productId);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("product name", products.getProductName());

        return new ResponseEntity(responseBody, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" +
                                    "\"productId\":\"1\","+
                                    "\"categoryId\":\"1\"," +
                                    "\"productName\":\"ban\"," +
                                    "\"productStatus\":\"true\"," +
                                    "\"price\":\"500000\"," +
                                    "\"description\":\"ban dengan kualitas terbaik\"," +
                                    "\"imageProduct\":\"string\"}"))
                    }),
            @ApiResponse(responseCode = "400", description = "failed!",
                    content = { @Content})})

    @Operation(summary = "this method function to get product data by user id")
    @GetMapping("/get-product-by-user/{userId}")
    public ResponseEntity<RestTemplate> getProductByUserId(@PathVariable Integer userId) {
        BaseResponse response = productsService.getProductByUserId(userId);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @Operation(summary = "this method function to get all product data")
    @GetMapping("/get-all-product")
    public ResponseEntity<RestTemplate> getAllProducts() {
        List<Products> allProducts = productsService.getAllProducts();
        return new ResponseEntity(allProducts, HttpStatus.OK);
    }

    @Operation(summary = "this method function to add product")
    @PostMapping(value = "/add-product",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RestTemplate> addProduct(@ModelAttribute ProductDTO products,
                                                   @RequestParam MultipartFile[] image) {
        ProductResponse productResponse = productsService.addProduct(products, image);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage("cant upload images above 4");
        if (image.length >= 5) {
            return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(productResponse, HttpStatus.OK);
    }

    @Operation(summary = "this method function to update product")
    @PutMapping("/update-product")
    public ResponseEntity<RestTemplate> updateProduct(@ModelAttribute Products products,
                                                      @RequestParam MultipartFile[] image) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage("cant upload images above 4");
        if (image.length >= 5) {
            return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(productsService.updateProduct(products, image),
                HttpStatus.OK);
    }

    @Operation(summary = "this method function to delete product")
    @DeleteMapping("/delete-product/{productId}")
    public ResponseEntity<RestTemplate> deleteProductByProductId(@PathVariable Integer productId) {
        CompletedResponse response = productsService.deleteProductByProductId(productId);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @Operation(summary = "this method function to get all product to homepage")
    @GetMapping("/get-home-page")
    public Page<Products> pagePagination(
            @RequestParam("page") int page,
            @RequestParam("size") int size) {
        return productsRepository.findAll(PageRequest.of(page, size));
    }

    @Operation(summary = "this method function to search and sort product")
    @GetMapping("/find-product")
    public Page<Products> findProduct(
            @RequestParam(defaultValue = "", required = false) String productName,
            @RequestParam(defaultValue = "0", required = false) Long priceMin,
            @RequestParam(defaultValue = "9999999999", required = false) Long priceMax,
            @RequestParam(defaultValue = "productName,asc", required = false) String[] sort,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size) {
        List<Order> orders = new ArrayList<>();
        if(sort[0].contains(",")) {
            for(String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                orders.add(new Order(Direction.fromString(_sort[1]), _sort[0]));
            }
        } else {
            orders.add(new Order(Direction.fromString(sort[1]), sort[0]));
        }
        return productsRepository.
             findByProductNameContainingIgnoreCaseAndPriceBetween
                     (productName, priceMin, priceMax, PageRequest.of(page, size, Sort.by(orders)));
    }

    @Operation(summary = "")
    @GetMapping("/find-category")
    public Page<Products> findCategory(
            @RequestParam(defaultValue = "", required = false) Integer categoryId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size) {

    return productsRepository.findByCategories(categoryId, PageRequest.of(page, size));
    }
}
