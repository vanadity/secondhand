package com.secondhand.mapping;

import com.secondhand.model.Offers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class OfferMapper {

    private Integer offerId;
    private Integer productId;
    private String productName;
    private Long price;
    private String buyer;
    private String seller;
    private List<String> imageProduct;

    public OfferMapper offerToDTO(Offers entity) {
        offerId = entity.getOfferId();
        productId = entity.getProducts().getProductId();
        productName = entity.getProducts().getProductName();
        price = Long.valueOf(String.valueOf(entity.getProducts().getPrice()));
        buyer = entity.getUsers().getEmail();
        seller = entity.getProducts().getUsers().getEmail();
        imageProduct = entity.getProducts().getImageProduct();
        return new OfferMapper(offerId, productId, productName, price, buyer, seller, imageProduct);
    }
}
