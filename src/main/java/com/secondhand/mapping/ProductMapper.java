package com.secondhand.mapping;

import com.secondhand.model.Products;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Controller;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Controller
public class ProductMapper {

    private Integer productId;
    private String productName;
    private String description;
    private Long price;
    private Boolean productStatus;
    private String categoryName;
    private Integer categoryId;
    private Integer userId;
    private List<String> imageProduct;

    public ProductMapper productToDTO(Products entity) {
        productId = entity.getProductId();
        productName = entity.getProductName();
        description = entity.getDescription();
        price = entity.getPrice();
        productStatus = entity.getProductStatus();
        categoryName = entity.getCategories().getCategoryName();
        categoryId = entity.getCategories().getCategoryId();
        userId = entity.getUsers().getUserId();
        imageProduct = entity.getImageProduct();

        return new ProductMapper(
                productId,
                productName,
                description,
                price,
                productStatus,
                categoryName,
                categoryId,
                userId,
                imageProduct
        );
    }
}
