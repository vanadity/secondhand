package com.secondhand.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity(name="users")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "email")
})

public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Integer userId;

    @Size(max = 25)
    @Column(name = "username")
    private String username;

    @Size(max = 25)
    @Column(name = "email")
    private String email;

    @Size(min = 8, max = 16)
    @Column(name="password")
    private String password;

    @Size(max = 25)
    @Column(name="city")
    private String city;

    @Size(max = 150)
    @Column(name = "address")
    private String address;

    @Size(min = 12, max = 13)
    @Column(name = "phone_number")
    private Long phoneNumber;

    @Column(name = "image_user")
    private String imageUser;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Roles> roles = new HashSet<>();

    public Users(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Users() {
    }
}