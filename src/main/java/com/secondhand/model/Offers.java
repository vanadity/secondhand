package com.secondhand.model;

import com.secondhand.enumeration.OfferStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "offers")
@Table(name = "offers", uniqueConstraints = {
        @UniqueConstraint(columnNames = "offer_id")
})
public class Offers implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_id")
    private Integer offerId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Users users;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    private Products products;

    @Column(name = "offer_price")
    private Long offerPrice;

    @Column(name = "offer_status")
    private OfferStatus offerStatus = OfferStatus.Available;
}