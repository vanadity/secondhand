package com.secondhand.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity(name = "products")
public class Products implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Integer productId;

    @ManyToOne(targetEntity = Categories.class)
    @JoinColumn(name = "category_id")
    private Categories categories;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Users users;

    @Column(name= "product_name")
    @Size(max = 25)
    private String productName;

    @Column(name = "product_status")
    private Boolean productStatus;

    @Column(name = "price")
    private Long price;

    @Column(name = "description")
    @Size(max = 100)
    private String description;

    @CreatedDate
    @Column(name = "created_date", nullable = false)
    private Date createdDate = new Date(System.currentTimeMillis());

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @CollectionTable(name = "image_product",
            joinColumns = @JoinColumn(name = "product_id"))
    private List<String> imageProduct;
}
