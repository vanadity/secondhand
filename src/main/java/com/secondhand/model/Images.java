package com.secondhand.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "images")
public class Images {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "image_id")
    private Integer imageId;

    @Column(name = "image_name")
    private String imageName;

    @Lob
    @Column(name = "image_file")
    private byte[] imageFile;

    public Images() {
    }

    public Images(String imageName, byte[] imageFile) {
        this.imageName = imageName;
        this.imageFile = imageFile;
    }
}
