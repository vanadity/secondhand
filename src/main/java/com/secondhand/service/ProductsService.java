package com.secondhand.service;

import com.secondhand.dto.ProductDTO;
import com.secondhand.model.Products;
import com.secondhand.response.BaseResponse;
import com.secondhand.response.CompletedResponse;
import com.secondhand.response.ProductResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface ProductsService {

    BaseResponse getProductByUserId(Integer userId);

    Products getProductByProductId(Integer productId);

    ProductResponse addProduct(ProductDTO productDTO, MultipartFile[] image);

    ProductResponse updateProduct(Products products, MultipartFile[] image);

    CompletedResponse deleteProductByProductId(Integer productId);

    List<Products> getAllProducts();
}
