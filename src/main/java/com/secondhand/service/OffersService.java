package com.secondhand.service;

import com.secondhand.dto.OffersDTO;
import com.secondhand.dto.OffersUpdateDTO;
import com.secondhand.mapping.OfferMapper;
import com.secondhand.model.Offers;
import com.secondhand.response.BaseResponse;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface OffersService {

    BaseResponse addOffers(OffersDTO offersDTO, Authentication authentication);

    OfferMapper updateOffers(OffersUpdateDTO offersUpdateDTO, Authentication authentication);

    List<OfferMapper> getOffersByUserId(Integer offerId);
}

