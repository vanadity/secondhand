package com.secondhand.service;

import com.secondhand.result.DataResult;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public interface CloudinaryService {

    DataResult<?> upload(MultipartFile multipartFile);

    DataResult<?> delete(String publicIdOfImage);
}
