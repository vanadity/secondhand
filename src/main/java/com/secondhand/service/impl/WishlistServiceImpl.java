package com.secondhand.service.impl;

import com.secondhand.dto.ProductDTO;
import com.secondhand.dto.ProfileUserDTO;
import com.secondhand.model.Wishlist;
import com.secondhand.repository.WishlistRepository;
import com.secondhand.service.WishlistService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class WishlistServiceImpl implements WishlistService {

    @Autowired
    private WishlistRepository wishlistRepository;

    @Override
    public String deleteWishlistById(Integer productId, Integer userId) {
        wishlistRepository.deleteWishlistByProductIdAndUserId(productId, userId);
        return "delete wishlsit id has been successful!";
    }

    @Override
    public List<Wishlist> getWishlistByUserId(Integer userId) {
        return null;
    }
}
