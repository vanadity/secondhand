package com.secondhand.service.impl;

import com.secondhand.dto.ProductDTO;
import com.secondhand.dto.ProfileUserDTO;
import com.secondhand.enumeration.OperationStatus;
import com.secondhand.exceptions.IllegalException;
import com.secondhand.mapping.ProductMapper;
import com.secondhand.model.Categories;
import com.secondhand.model.Products;
import com.secondhand.model.Users;
import com.secondhand.repository.CategoriesRepository;
import com.secondhand.repository.ProductsRepository;
import com.secondhand.repository.UsersRepository;
import com.secondhand.response.BaseResponse;
import com.secondhand.response.CompletedResponse;
import com.secondhand.response.ProductResponse;
import com.secondhand.service.CloudinaryService;
import com.secondhand.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductsService {

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private CategoriesRepository categoriesRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private CloudinaryService cloudinaryService;

    @Autowired
    public ProductServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public BaseResponse getProductByUserId(Integer userId) {
        List<ProductMapper> collect = productsRepository.findProductByUserId(userId)
                .stream()
                .map(productMapper::productToDTO)
                .collect(Collectors.toList());

        return new BaseResponse(HttpStatus.ACCEPTED, "product found", collect);
    }

    @Override
    public Products getProductByProductId(Integer productId) {
        return productsRepository.findByProductId(productId);
    }

    @Override
    public ProductResponse addProduct(ProductDTO productDTO, MultipartFile[] image) {
        List<String> images = new ArrayList<>();
        Products products = new Products();
        products.setProductName(productDTO.getProductName());
        products.setProductStatus(productDTO.getProductStatus());
        products.setPrice(productDTO.getPrice());
        products.setDescription(productDTO.getDescription());

        Arrays.stream(image).limit(4)
                .filter(file -> {
                    if (file.isEmpty()) {
                        throw new IllegalException("The file is required to create a new");
                    }
                    return true;
                }).forEach(file -> {
                    Map<?, ?> uploadResult =
                            (Map<?, ?>) cloudinaryService.upload(file).getData();
                    images.add(uploadResult.get("url").toString());
                });
        Categories categories = categoriesRepository.findByCategoryId(productDTO.getCategoryId());
        Users users = usersRepository.findByUserId(productDTO.getUserId());
        products.setCategories(categories);
        products.setUsers(users);
        products.setImageProduct(images);

        productsRepository.save(products);
        return new ProductResponse(
                products.getProductName(),
                products.getProductStatus(),
                products.getPrice(),
                products.getDescription(),
                products.getCategories().getCategoryName(),
                products.getUsers().getUsername(),
                products.getCreatedDate().toString(),
                images);
    }

    @Override
    public ProductResponse updateProduct(Products products, MultipartFile[] image) {
        Products updateProduct = productsRepository.findByProductId(products.getProductId());
        updateProduct.setProductName(products.getProductName());
        updateProduct.setProductStatus(products.getProductStatus());
        updateProduct.setPrice(products.getPrice());
        updateProduct.setDescription(products.getDescription());
        List<String> images = new ArrayList<>();
        Arrays.stream(image).limit(4)
                .filter(file -> {
                    if (file.isEmpty()) {
                        throw new IllegalException("The file is required to create a new");
                    }
                    return true;
                }).forEach(file -> {
                    Map<?, ?> uploadResult =
                            (Map<?, ?>) cloudinaryService.upload(file).getData();
                    images.add(uploadResult.get("url").toString());
                });
        products.setImageProduct(images);

        productsRepository.save(updateProduct);
        return new ProductResponse(
                updateProduct.getProductName(),
                updateProduct.getProductStatus(),
                updateProduct.getPrice(),
                updateProduct.getDescription(),
                updateProduct.getCategories().getCategoryName(),
                updateProduct.getUsers().getUsername(),
                updateProduct.getCreatedDate().toString(),
                images);
    }

    @Override
    public CompletedResponse deleteProductByProductId(Integer productId) {
        productsRepository.deleteById(productId);
        return new CompletedResponse("request has been successful",
                OperationStatus.SUCCESS.getName());
    }

    @Override
    public List<Products> getAllProducts() {
        return productsRepository.findAll();
    }
}
