package com.secondhand.service.impl;

import com.secondhand.model.Categories;
import com.secondhand.repository.CategoriesRepository;
import com.secondhand.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriesServiceImpl implements CategoriesService {

    @Autowired
    CategoriesRepository categoriesRepository;

    @Override
    public Categories getCategoryByCategoryName(String categoryName) {
        return categoriesRepository.findByCategoryName(categoryName);
    }

    @Override
    public Categories getCategoryByCategoryId(Integer categoryId) {
        return categoriesRepository.findByCategoryId(categoryId);
    }

    @Override
    public Categories addCategory(Categories categories) {
        categories.setCategoryName(categories.getCategoryName());
        return categoriesRepository.save(categories);
    }

    @Override
    public Categories updateCategory(Categories categories) {
        Categories updateCategory = categoriesRepository.findByCategoryName(categories.getCategoryName());
        updateCategory.setCategoryName(categories.getCategoryName());
        return categoriesRepository.save(updateCategory);
    }

    @Override
    public String deleteCategoryByCategoryId(Integer categoryId) {
        categoriesRepository.deleteById(categoryId);
        return "delete category id " + categoryId + " has been successful!";
    }

    @Override
    public List<Categories> getAllCategories() {
        return categoriesRepository.findAll();
    }
}
