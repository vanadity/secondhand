package com.secondhand.service.impl;

import com.secondhand.dto.OffersDTO;
import com.secondhand.dto.OffersUpdateDTO;
import com.secondhand.enumeration.OfferStatus;
import com.secondhand.exceptions.BaseException;
import com.secondhand.mapping.OfferMapper;
import com.secondhand.model.Offers;
import com.secondhand.model.Products;
import com.secondhand.model.Users;
import com.secondhand.repository.OffersRepository;
import com.secondhand.response.BaseResponse;
import com.secondhand.service.OffersService;
import com.secondhand.service.ProductsService;
import com.secondhand.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class OffersServiceImpl implements OffersService {

    @Autowired
    private ProductsService productsService;

    @Autowired
    private UsersService usersService;

    @Autowired
    private OfferMapper offerMapper;

    @Autowired
    private OffersRepository offersRepository;

    @Override
    public BaseResponse addOffers(OffersDTO offersDTO, Authentication authentication) {
        Users buyer = usersService.getUserByUsername(authentication.getName());
        Products products = productsService.getProductByProductId(offersDTO.getProductId());
        Integer seller = products.getUsers().getUserId();
        Offers offers = new Offers();

        offers.setUsers(buyer);
        offers.setProducts(products);
        offers.setOfferPrice(offersDTO.getOfferPrice());
        if (Objects.deepEquals(buyer.getUserId(), seller)) {
            return new BaseResponse(HttpStatus.BAD_REQUEST,
                    "you cant bid on your own product", null);
        }
        offersRepository.save(offers);
        return new BaseResponse(HttpStatus.ACCEPTED,
                "bid has been successfully", offerMapper.offerToDTO(offers));
    }

    @Override
    public List<OfferMapper> getOffersByUserId(Integer userId) {
        return offersRepository.findByUserId(userId).stream().map(offerMapper::offerToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public OfferMapper updateOffers(OffersUpdateDTO offersUpdateDTO, Authentication authentication) {
        Offers updateOffers = offersRepository.findById(offersUpdateDTO.getOfferId())
                .orElseThrow(() -> new BaseException("offer not found"));

        Integer sellerId = updateOffers.getProducts().getUsers().getUserId();

        if (offersUpdateDTO.getOfferStatus().equals(OfferStatus.Accepted)) {
            updateOffers.setOfferStatus(OfferStatus.Accepted);

            offersRepository.save(updateOffers);
        } else if (offersUpdateDTO.getOfferStatus().equals(OfferStatus.Rejected)) {
            updateOffers.setOfferStatus(OfferStatus.Rejected);

            offersRepository.save(updateOffers);
        }
        return offerMapper.offerToDTO(updateOffers);
    }
}
