package com.secondhand.service.impl;

import com.secondhand.dto.ProfileUserDTO;
import com.secondhand.model.Users;
import com.secondhand.repository.UsersRepository;
import com.secondhand.response.ProfileUserResponse;
import com.secondhand.service.CloudinaryService;
import com.secondhand.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private CloudinaryService cloudinaryService;

    @Override
    public Users getUserByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    @Override
    public Users getUserById(Integer userId) {
        return usersRepository.findByUserId(userId);
    }

    @Override
    public Users getUserByEmail(String email) {
        return usersRepository.findByEmail(email);
    }

    @Override
    public Users addUser(Users users) {
        users.setUsername(users.getUsername());
        users.setEmail(users.getEmail());
        users.setPassword(users.getPassword());
        return usersRepository.save(users);
    }

    @Override
    public ProfileUserResponse updateUserbyId(ProfileUserDTO profileUserDTO, MultipartFile image) {
        Users updateUser =
                usersRepository.findById(profileUserDTO.getUserId()).orElseThrow(() -> new UsernameNotFoundException(
                String.format("",profileUserDTO.getUserId()))
        );

        if (image != null && !image.isEmpty()){
            Map<?, ?> uploadImage = (Map<?, ?>) cloudinaryService.upload(image).getData();

            profileUserDTO.setImageUser(uploadImage.get("url").toString());
            updateUser.setImageUser(profileUserDTO.getImageUser());
        }

        updateUser.setUsername(profileUserDTO.getUsername());
        updateUser.setCity(profileUserDTO.getCity());
        updateUser.setAddress(profileUserDTO.getAddress());
        updateUser.setPhoneNumber(profileUserDTO.getPhoneNumber());
        usersRepository.save(updateUser);

        return new ProfileUserResponse(
                updateUser.getUserId(),
                updateUser.getUsername(),
                updateUser.getCity(),
                updateUser.getAddress(),
                updateUser.getPhoneNumber(),
                updateUser.getImageUser()
        );
    }

    @Override
    public String deleteUserByUserId (Integer userId) {
        usersRepository.deleteById(userId);
        return "delete user id " + userId + " has been successful!";
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }
}

