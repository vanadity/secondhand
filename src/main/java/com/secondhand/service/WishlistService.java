package com.secondhand.service;

import com.secondhand.model.Wishlist;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WishlistService {

//    Wishlist addWishlist(Wishlist wishlist);

    String deleteWishlistById(Integer productId, Integer userId);

    List<Wishlist> getWishlistByUserId(Integer userId);
}
