package com.secondhand.service;

import com.secondhand.model.Categories;
import com.secondhand.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoriesService {

    Categories getCategoryByCategoryName(String categoryName);

    Categories getCategoryByCategoryId(Integer categoryId);

    Categories addCategory(Categories categories);

    Categories updateCategory(Categories categories);

    String deleteCategoryByCategoryId(Integer categoryId);

    List<Categories> getAllCategories();
}
