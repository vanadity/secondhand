package com.secondhand.service;

import com.secondhand.dto.ProfileUserDTO;
import com.secondhand.model.Users;
import com.secondhand.response.ProfileUserResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface UsersService {

    Users getUserByUsername(String username);
    
    Users getUserById(Integer userId);

    Users getUserByEmail(String email);

    Users addUser(Users users);

    ProfileUserResponse updateUserbyId(ProfileUserDTO profileUserDTO, MultipartFile image);

    String deleteUserByUserId(Integer userId);

    List<Users> getAllUsers();
}
