package com.secondhand;

import com.secondhand.controller.UsersController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UsersTest {

    @Autowired
    private UsersController usersController;

    /** positive test case */
    @Test
    @DisplayName("get user by username")
    public void getUserByUsername() {
//        String response = usersController.getUserByUsername("Ivan");
    }

    @Test
    @DisplayName("add user")
    public void addUser() {
//        String response = usersController.addUser("adit", "adit@email.com", "passAdit");
    }

    @Test
    @DisplayName("update user")
    public void updateUser() {
//        String response = usersController.updateUser(
//                "Ivan", "Ivan Aditya",
//                "ivanaditya@email.com", "passIvanAditya");
    }

    @Test
    @DisplayName("delete user")
    public void deleteUser() {
//        String response = usersController.deleteUserByUserId(1);
    }
}
