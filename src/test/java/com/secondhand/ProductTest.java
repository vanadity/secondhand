package com.secondhand;

import com.secondhand.model.Products;
import com.secondhand.repository.ProductsRepository;
import com.secondhand.service.impl.ProductServiceImpl;
import com.secondhand.service.ProductsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ProductTest {

    @Mock
    private ProductsRepository productsRepository;

    private ProductsService productsService;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
        this.productsService = new ProductServiceImpl(this.productsRepository);
    }

    @Test
    @DisplayName("get product by productName")
    public void getProductByProductName() {

    }

    @Test
    @DisplayName("add product")
    public void addProduct() {
        Products products = new Products();
        products.setProductId(5);
        products.setProductName("handphone");
        products.setProductStatus(true);
        products.setDescription("bekas");
        products.setPrice(Long.valueOf("25000"));

        assertThat(products.getProductId()).isEqualTo(5);
        assertThat(products.getProductName()).isEqualTo("handphone");
        assertThat(products.getProductStatus()).isEqualTo(true);
        assertThat(products.getDescription()).isEqualTo("bekas");
        assertThat(products.getPrice()).isEqualTo(Long.valueOf("25000"));
    }

    @Test
    @DisplayName("delete product")
    public void deleteProductByProductId() {
        Products products = new Products();
        products.setProductId(10);

        productsService.deleteProductByProductId(products.getProductId());
        Mockito.verify(productsRepository).deleteById(products.getProductId());
    }
}
